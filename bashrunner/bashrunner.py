from argparse import ArgumentParser, REMAINDER
from BashContainer import ContainerManager, Container
from os.path import expanduser, join


SETTINGS_FILE_NAME = ".bashRunnerSettings.json"

class BashRunner:

    def __init__(self, args, path):
        self.args = args
        self.containerManager = ContainerManager(path)

    # Ejecuta los comandos
    def run(self):
        command = self.args

        if command.current:
            self.__printCurrentContainer()
        elif command.create != None:
            self.__createContainer(command.create)
        elif command.set != None:
            self.__setCurrentContainer(command.set)
        elif command.list:
            self.__printListOfContainers()
        elif command.delete != None:
            self.__deleteContainers(command.delete)
        elif command.add != None:
            self.__addCommand(command.add)
        elif command.dump != None:
            self.__dumpContainers(command.dump)
        elif command.run != None:
            self.__runContainers(command.run, command.workspace)

    # Internal
    # --------------------------------------------------------------------------


    # Imprime el nombre del contenedor actual
    def __printCurrentContainer(self):
        currentContainer = self.containerManager.currentContainer
        if currentContainer == None:
            print("Actualmente no hay ningún cotenedor configurado")
        else:
            print("El contenedor actual es {}".format(currentContainer))


    # Crea un contenedor vacío
    def __createContainer(self, name):
        if self.containerManager.getContainerIndex(name) != None:
            print("No se puede crear el contenedor {} pues este ya existe".format(name))
        else:
            newContainer = Container(name, [])
            self.containerManager.containers.append(newContainer)
            self.containerManager.writeSettingsFile()
            print("Se ha creado el contenendor {} exitosamente".format(name))

    # Establece el contenedor actual
    def __setCurrentContainer(self, name):
        if self.containerManager.getContainerIndex(name) == None:
            print("No se puede configurar el contenedor {} pues este no existe".format(name))
        else:
            self.containerManager.currentContainer = name
            self.containerManager.writeSettingsFile()
            print("El contenedor actual es {}".format(name))

    # Imprime la lista de contenedores disponibles
    def __printListOfContainers(self):
        containers = self.containerManager.containers
        if len(containers) == 0:
            print("No hay contenedores")
        else:
            print("Contenedores disponibles: ")
            for container in containers:
                print("- {}".format(container.name))

    # Elimina uno o varios contenedores
    def __deleteContainers(self, containers):
        # Si la lista está vacía configura como contenedores el contenedor
        # actual de ser posible. Si no puede da una lista vacía
        if len(containers) == 0:
            if self.containerManager.currentContainer == None:
                containers = []
            else:
                containers = [self.containerManager.currentContainer]

        # Elimina los contenedores
        while len(containers) != 0:
            name = containers.pop(0)
            index = self.containerManager.getContainerIndex(name)
            # Elimina so es posible
            if index == None:
                print("El contenedor {} no existe".format(name))
            else:
                self.containerManager.containers.pop(index)
                print("Eliminando contenedor {}".format(name))

            # Quita el contenedor por defecto si es el caso
            if self.containerManager.currentContainer == name:
                self.containerManager.currentContainer = None
                print("Quitando contenedor {} como contenedor por defecto".format(name))
        self.containerManager.writeSettingsFile()
        print("Eliminaciones concluidas")

    # Agrega un comando al contenedor actual de ser posible
    def __addCommand(self, command):
        currentContainerName = self.containerManager.currentContainer
        if currentContainerName == None:
            print("Actualmente no hay ningún cotenedor configurado")
            return

        index = self.containerManager.getContainerIndex(currentContainerName)
        currentContainer = self.containerManager.containers[index]

        currentContainer.addCommand(command)
        self.containerManager.writeSettingsFile()

    # Imprime los comandos de un grupo de contenedores
    def __dumpContainers(self, containers):
        if len(containers) == 0:
            if self.containerManager.currentContainer == None:
                containers = []
            else:
                containers = [self.containerManager.currentContainer]

        # A partir de acá tenemos una lista de contenedores para trabajar
        for containerName in containers:
            index = self.containerManager.getContainerIndex(containerName)

            if index == None:
                print("El contenedor {} no existe".format(containerName))
            else:
                container = self.containerManager.containers[index]
                container.dumpData()

    # Corre los contenedores indicados en un espacio de trabajo
    def __runContainers(self, containers, workspace):
        if len(containers) == 0:
            if self.containerManager.currentContainer == None:
                containers = []
            else:
                containers = [self.containerManager.currentContainer]

        # A partir de acá tenemos una lista de contenedores para trabajar
        for containerName in containers:
            index = self.containerManager.getContainerIndex(containerName)

            if index == None:
                print("El contenedor {} no existe".format(containerName))
            else:
                container = self.containerManager.containers[index]
                container.run(workspace)

# Argument Parser
# ------------------------------------------------------------------------------

def parseArguments():
    parser = ArgumentParser()
    parser.add_argument("--current",
                        action="store_true",
                        help="Muestra el nombre del contenedor actual.")
    parser.add_argument("--delete", "-d",
                        nargs="*",
                        help="Elimina un contenedor. En caso de no indicarse cual se tomará el contenedor actual.")
    parser.add_argument("--create", "-c",
                        type=str,
                        help="Crea un contenedor.")
    parser.add_argument("--set", "-s",
                        type=str,
                        help="Establece el contenedor como el contendor actual.")
    parser.add_argument("--dump",
                        nargs="*",
                        help="Muesta el contenido de un contenedor. En caso de no indicarse cúal se utilizará el contenedor actual de ser posible")
    parser.add_argument("--list", "-l",
                        action="store_true",
                        help="Lista los contenedores existentes.")
    parser.add_argument("--run", "-r",
                        nargs="*",
                        help="Ejecuta los comandos de un grupo de contenedores. En caso de no indicarse cuál se utilizará el contenedor actual de ser posible")
    parser.add_argument("--workspace", "-w",
                        type=str,
                        help="Configura el directorio de ejecución")
    parser.add_argument("--add", "-a",
                        nargs=REMAINDER,
                        help="Agrega un comando al contenedor actual.")
    return parser.parse_args()

# Main
# ------------------------------------------------------------------------------

def main():
    args = parseArguments()

    # Genera la ruta del archivo de configuración
    global SETTINGS_FILE_NAME
    home = expanduser("~")
    settingsFilePath = join(home, SETTINGS_FILE_NAME)

    bashRunner = BashRunner(args, settingsFilePath)
    bashRunner.run()


if __name__ == "__main__":
    main()
