# bashrunner
* El comando almacenará contenedores de instrucciones
* Las instrucciones son comandos disponibles en sh (no alias)


* Habrá una lista de contenedores con los comandos necesarios
* Habrá un contenedor configurado como si hubiera un cursor

## Contenedores
Un contenedor es una estructura que almacena la lista de comandos ejcutadas en una sesion.
### Información a almacenar en un contenedor
* Nombre (Identificador único)
* Lista de comandos


## Acciones disponibles para un contenedor:


Comando | Funcionalidad
---|---
`--current` | Obtiene el nombre del contenedor actual.
`--delete`, `-d` | Elimina el contenedor actual
`--delete` [nombre] | Elimina un contenedor.
`--create`, `-c` [nombre] | Crea un contenedor.
`--set`, `-s` [nombre] | Establece un contenedor como el actual (en base al nombre).
`--dump` [nombre] | Muesta la lista de comandos en un contenedor.
`--dump` | Muestra la lista de comandos del contenedor actual.
`--list`, `-l` | Muestra la lista de contenedores.    
`--run`, `-r` [nombre] | Corre los comandos de un contenedor.
`--add`, `-a` [comando] | Agrega un comando al contenedor actual.

** ACLARACIÓN ** : <nombre> hace referencia al nombre de un contenedor

### Acciones extras a considerar
* vaciar contendor (actual o específico)
* vaciar lista de contenedores

## Cómo se almacenarán los contenedores
Para almacenar contenedores se usará un archivo oculto (unix like) donde están todas las configuraciones necesarias.

** ADVERTENCIA ** : Al tratarse de un archivo en texto plano (mas especificamente formato JSON) no debe ingresarse información sensible.
